FROM ubuntu:18.04
RUN pwd

RUN echo "Updating OS..."
RUN apt-get -y update && \
    apt-get -y install sudo

RUN sudo apt-get -y install git

RUN echo "Installing openjdk..."
#RUN sudo apt-add-repository ppa:openjdk-r/ppa
#RUN apt-get update -y
RUN sudo apt-get install -y openjdk-8-jdk
#RUN sudo apt-get install -y openjdk-11-jdk-headless

RUN echo "Installing maven..."
RUN sudo apt-get install -y maven

RUN echo "Installing SSH..."
# Make ssh dir
RUN mkdir /root/.ssh/

# Copy over private key, and set permissions
# Warning! Anyone who gets their hands on this image will be able
# to retrieve this private key file from the corresponding image layer
#RUN ssh-keygen -y
#ADD id_rsa /root/.ssh/id_rsa

# Create known_hosts
#RUN touch /root/.ssh/known_hosts
# Add bitbuckets key
#RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

# Clone the conf files into the docker container
#RUN git clone https://grathor33@bitbucket.org/grathor33/hellow.git
#############RUN git clone git@bitbucket.org:grathor33/hellow.git

RUN useradd -m docker && echo "docker:docker" | chpasswd && adduser docker sudo

USER docker
#CMD /bin/bash
RUN pwd

RUN cd /home/docker
RUN chown -R docker /home/docker
#########RUN git clone -b docker https://grathor33@bitbucket.org/grathor33/hellow.git
####RUN cd hellow

########COPY . /app

#RUN sudo touch logging.log
#RUN sudo java --version
#>> logging.log
#RUN sudo mvn --version

RUN echo "We are here..."
RUN pwd
RUN echo "Compiling the project..."
#RUN sudo javac HellowApp.java

#RUN sudo cd .......

#RUN sudo rm -r target
RUN mvn compile

RUN pwd
RUN echo "Building the project..."
RUN mvn package
RUN mvn test

RUN pwd
RUN echo "Installing the project..."
RUN mvn install
# copy WAR into image
COPY ./target/hellow-1.0.1.jar /app.war

RUN pwd
RUN echo "Running the project..."
CMD sudo java HellowApp
# run application with this command line
#CMD ["/usr/bin/java", "-jar", "-Dspring.profiles.active=default", "/app.war"]
