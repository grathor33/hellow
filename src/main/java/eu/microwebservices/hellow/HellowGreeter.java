package eu.microwebservices.hellow;

public class HellowGreeter {

    public static final String GREETING = "Hello world!";

    public String sayHello() {
        return GREETING;
    }
}
