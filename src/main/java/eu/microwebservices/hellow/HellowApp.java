package eu.microwebservices.hellow;

import org.joda.time.LocalTime;

public class HellowApp {

    public static final String LOCAL_TIME = "The current local time is: ";

	public static void main(String[] args) {
		LocalTime currentTime = new LocalTime();
		System.out.println(LOCAL_TIME + currentTime);

		HellowGreeter greeter = new HellowGreeter();
		System.out.println(greeter.sayHello());
	}

}

